import java.util.stream.IntStream;

/**
 * 1st exercise.
 */
public class PrimeChecker {

    public static boolean isPrime(int number) {
        boolean divisible = false;

        return number > 1 &&
                IntStream
                        .rangeClosed(2, (int) Math.sqrt(number))
                        .parallel()
                        .noneMatch(index -> number % index == 0);
    }

    public static void main(String[] args) {
        for (int i = 1; i < 12; i++) {
            System.out.println(String.format("isPrime(%d)? %b", i, isPrime(i)));
        }
    }
}
