package id.ac.ui.cs.advprog.tutorial4.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.*;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.*;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.*;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.*;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.*;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import org.junit.Before;
import org.junit.Test;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class FactoryTest {

    private Cheese[] Cheeses;
    private Clams[] Clamss;
    private Dough[] Doughs;
    private Sauce[] Sauces;
    private Veggies[] Veggiess;

    @Before
    public void setUp() {
        //Cheese
        Cheeses = new Cheese[4];
        Cheeses[0] = new MozzarellaCheese();
        Cheeses[1] = new ParmesanCheese();
        Cheeses[2] = new ReggianoCheese();
        Cheeses[3] = new PranCheese();

        //Clam
        Clamss = new Clams[3];
        Clamss[0] = new FreshClams();
        Clamss[1] = new FrozenClams();
        Clamss[2] = new ThunderClams();

        //Dough
        Doughs = new Dough[3];
        Doughs[0] = new ThickCrustDough();
        Doughs[1] = new ThinCrustDough();
        Doughs[2] = new HotDough();

        //Sauce
        Sauces = new Sauce[3];
        Sauces[0] = new PlumTomatoSauce();
        Sauces[1] = new MarinaraSauce();
        Sauces[2] = new TartarSauce();

        //Veggies
        Veggiess = new Veggies[8];
        Veggiess[0] = new BlackOlives();
        Veggiess[1] = new Eggplant();
        Veggiess[2] = new Garlic();
        Veggiess[3] = new Mushroom();
        Veggiess[4] = new Onion();
        Veggiess[5] = new RedPepper();
        Veggiess[6] = new Spinach();
        Veggiess[7] = new Taro();

    }

    @Test
    public void testToString() {
        //CheeseString
        assertEquals("Shredded Mozzarella", Cheeses[0].toString());
        assertEquals("Shredded Parmesan", Cheeses[1].toString());
        assertEquals("Reggiano Cheese", Cheeses[2].toString());
        assertEquals("PranCheese from France", Cheeses[3].toString());

        //Clam
        assertEquals("Fresh Clams from Long Island Sound", Clamss[0].toString());
        assertEquals("Frozen Clams from Chesapeake Bay", Clamss[1].toString());
        assertEquals("Thunder Clams from Deppen", Clamss[2].toString());

        //Dough
        assertEquals("ThickCrust style extra thick crust dough", Doughs[0].toString());
        assertEquals("Thin Crust Dough", Doughs[1].toString());
        assertEquals("Hot style extra hot dough", Doughs[2].toString());

        //Sauce
        assertEquals("Tomato sauce with plum tomatoes", Sauces[0].toString());
        assertEquals("Marinara Sauce", Sauces[1].toString());
        assertEquals("Tartar sauce with tar", Sauces[2].toString());

        //Veggies
        assertEquals("Black Olives", Veggiess[0].toString());
        assertEquals("Eggplant", Veggiess[1].toString());
        assertEquals("Garlic", Veggiess[2].toString());
        assertEquals("Mushrooms", Veggiess[3].toString());
        assertEquals("Onion", Veggiess[4].toString());
        assertEquals("Red Pepper", Veggiess[5].toString());
        assertEquals("Spinach", Veggiess[6].toString());
        assertEquals("Taro", Veggiess[7].toString());

    }
}
