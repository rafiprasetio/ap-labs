package id.ac.ui.cs.advprog.tutorial4.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import org.junit.Test;

import static org.junit.Assert.*;

public class DepokFactoryTest {

    @Test
    public void testCanCreatePizza() {
        PizzaStore joniaStore = new DepokPizzaStore();

        Pizza myPizza = joniaStore.orderPizza("cheese");
        assertNotNull(myPizza);
        assertEquals("---- Depok Style Cheese Pizza ----\n" +
                "Hot style extra hot dough\n" +
                "Tartar sauce with tar\n" +
                "PranCheese from France\n", myPizza.toString());

        myPizza = joniaStore.orderPizza("clam");
        assertNotNull(myPizza);
        assertEquals("---- Depok Style Clam Pizza ----\n" +
                "Hot style extra hot dough\n" +
                "Tartar sauce with tar\n" +
                "PranCheese from France\n" +
                "Thunder Clams from Deppen\n", myPizza.toString());

        myPizza = joniaStore.orderPizza("veggie");
        assertNotNull(myPizza);
        assertEquals("---- Depok Style Veggie Pizza ----\n" +
                "Hot style extra hot dough\n" +
                "Tartar sauce with tar\n" +
                "PranCheese from France\n" +
                "Black Olives, Eggplant, Taro, Red Pepper\n", myPizza.toString());
    }
}
