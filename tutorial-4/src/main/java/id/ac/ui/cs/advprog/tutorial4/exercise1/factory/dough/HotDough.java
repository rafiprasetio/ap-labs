package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class HotDough implements Dough {
    public String toString() {
        return "Hot style extra hot dough";
    }
}
