package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

public class PranCheese implements Cheese {

    public String toString() {
        return "PranCheese from France";
    }
}
