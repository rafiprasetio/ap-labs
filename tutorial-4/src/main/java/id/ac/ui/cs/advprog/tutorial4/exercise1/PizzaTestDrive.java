package id.ac.ui.cs.advprog.tutorial4.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;

public class PizzaTestDrive {

    public static void main(String[] args) {
        PizzaStore nyStore = new NewYorkPizzaStore();

        Pizza pizza = nyStore.orderPizza("cheese");
        System.out.println("Ethan ordered a " + pizza + "\n");

        pizza = nyStore.orderPizza("clam");
        System.out.println("Ethan ordered a " + pizza + "\n");

        pizza = nyStore.orderPizza("veggie");
        System.out.println("Ethan ordered a " + pizza + "\n");

        // TODO Complete me!
        // Create a new Pizza Store franchise at Depok
        
        PizzaStore joniaStore = new DepokPizzaStore();
        
        Pizza pizza2 = joniaStore.orderPizza("cheese");
        System.out.println("Mas Imron ordered a " + pizza2 + "\n");

        pizza2 = joniaStore.orderPizza("clam");
        System.out.println("Mas Imron ordered a " + pizza2 + "\n");

        pizza2 = joniaStore.orderPizza("veggie");
        System.out.println("Mas Imron ordered a " + pizza2 + "\n");
    }
}
