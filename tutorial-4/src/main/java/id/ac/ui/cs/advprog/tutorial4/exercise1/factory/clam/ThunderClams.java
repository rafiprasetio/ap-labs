package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class ThunderClams implements Clams {

    public String toString() {
        return "Thunder Clams from Deppen";
    }
}
