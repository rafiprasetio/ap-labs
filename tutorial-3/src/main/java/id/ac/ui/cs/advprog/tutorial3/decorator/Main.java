package id.ac.ui.cs.advprog.tutorial3.decorator;
import id.ac.ui.cs.advprog.tutorial3.decorator.bread.*;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.*;

public class Main {
	public static void main(String args[]) {
		ThickBunBurger thick = new ThickBunBurger();
		Cheese cheese = new Cheese(thick);
		System.out.println(cheese.getDescription());
		System.out.println(cheese.cost());
		CrustySandwich crusty = new CrustySandwich();
		BeefMeat beef = new BeefMeat(crusty);
		System.out.println(beef.getDescription());
		System.out.println(beef.cost());
	}
}
