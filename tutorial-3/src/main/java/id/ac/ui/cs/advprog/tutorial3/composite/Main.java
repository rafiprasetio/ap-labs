package id.ac.ui.cs.advprog.tutorial3.composite;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.*;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.*;

public class Main {
	public static void main(String args[]) {
		Company company = new Company();
		Ceo ceo = new Ceo("Rafi", 250000);
		Cto cto = new Cto("Mas Imron", 100000);
		FrontendProgrammer fp = new FrontendProgrammer("Mas Singham",33333);
		BackendProgrammer bp = new BackendProgrammer("Mas Cobra",23232);
		NetworkExpert ne = new NetworkExpert("Mas Fahmi",50000);
		SecurityExpert se = new SecurityExpert("Mas Caraval",75000);
		UiUxDesigner uud = new UiUxDesigner("Mas Indra", 99999);
		
		company.addEmployee(ceo);
		company.addEmployee(cto);
		company.addEmployee(fp);
		company.addEmployee(bp);
		company.addEmployee(ne);
		company.addEmployee(se);
		company.addEmployee(uud);
		
		for(int i=0; i<company.employeesList.size(); i++) {
			System.out.println("Nama: " + company.employeesList.get(i).getName()+ " - Role: " + company.employeesList.get(i).getRole() + " - Gaji: "+ company.getAllEmployees().get(i).getSalary());
		}
		
		System.out.println("Total Gaji Perusahaan: " + company.getNetSalaries());
		
	}

}
