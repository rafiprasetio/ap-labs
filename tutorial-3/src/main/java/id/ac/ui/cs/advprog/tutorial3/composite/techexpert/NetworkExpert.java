package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class NetworkExpert extends Employees {
    public NetworkExpert(String name, double salary) {
        //TODO Implement
    	if (salary < 50000) {
    		throw new IllegalArgumentException("Invalid Salary "+ salary);
    	}
    	this.name = name;
    	this.salary = salary;
    	this.role = "Network Expert";
    }

    @Override
    public double getSalary() {
        //TODO Implement
    	return salary;
    }
}
