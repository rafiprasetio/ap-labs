package sorting;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class SortAndSearchTest {
    private static int[] unsortedArray = {0, 3, 5, 1, 4, 7, 6, 2, 9, 8};
    private static int[] sortedArray = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

    @Test
    public void slowSortTest() {
        int[] arr = unsortedArray;
        Sorter.slowSort(arr);
        for (int i = 0; i < unsortedArray.length; i++) {
            assertEquals(arr[i], sortedArray[i]);
        }
    }

    @Test
    public void fastSortTest() {
        int[] arr = unsortedArray;
        Sorter.fastSort(arr);
        for (int i = 0; i < unsortedArray.length; i++) {
            assertEquals(arr[i], sortedArray[i]);
        }
    }

    @Test
    public void slowSearchTest() {
        int search = 7;
        assertEquals(search, Finder.slowSearch(unsortedArray, search));
        assertEquals(-1, Finder.slowSearch(unsortedArray, 10));
    }

    @Test
    public void binarySearchTest() {
        int search = 7;
        assertEquals(search, Finder.binarySearch(unsortedArray, search));
        assertEquals(-1, Finder.slowSearch(unsortedArray, 10));
    }
}
